module.exports.defaultOptions = {
  signature: true,
  acme: {
    redirect: '',
    store: '',
    webroot: ''
  },
  log: {
    level: 'info'
  },
  workers: {
    count: 'max_physical_cpu_cores'
  },
  http: {
    redirect: true,
    from: 8080,
    to: 8443
  },
  https: {
    port: 8443,
    key: '',
    cert: '',
    ca: []
  },
  hosts: []
}
