const {getHost} = require('./getHost')

module.exports.getOrigin = (request) => {
  const protocol = request.headers['x-forwarded-proto'] ||
    request.headers[':scheme'] ||
    'https'

  const host = getHost(request)

  return `${protocol}://${host}`
}
