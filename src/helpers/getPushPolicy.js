function normalise (policy) {
  return policy
    .trim()
    .toLowerCase()
}

const cache = new WeakMap()

module.exports.getPushPolicy = (request, preferred) => {
  let available

  if (cache.has(request)) {
    available = cache.get(request)
  } else {
    const header = request.headers['accept-push-policy']
    const tokens = header ? header.split(',').map(normalise) : []
    available = new Set(tokens)
    cache.set(request, available)
  }

  for (const policy of available) {
    if (preferred.includes(policy)) {
      return policy
    }
  }
}
