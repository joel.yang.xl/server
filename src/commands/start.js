const {Master} = require('../master')
const debug = require('debug')
const opn = require('opn')
const {Configuration} = require('../configuration/Configuration')

module.exports.command = ['start']
module.exports.aliases = ['run', 'up', 'launch']
module.exports.desc = 'Start the server'
module.exports.builder = {
  config: {
    type: 'string',
    describe: 'Configuration file'
  },
  root: {
    type: 'string',
    describe: 'Directory to serve'
  },
  port: {
    type: 'number',
    describe: 'Listening port'
  },
  loglevel: {
    type: 'string',
    describe: 'Output level'
  },
  open: {
    type: 'boolean',
    describe: 'Open in browser'
  }
}

module.exports.handler = async (argv) => {
  const configuration = new Configuration(argv)
  const options = await configuration.load({generateCertificate: true})

  if (options.log.level !== 'silent') debug.enable(process.title)

  const master = new Master(options, argv)
  await master.start()

  if (argv.open) {
    const host = options.hosts[0].domain
    const port = options.https.port
    opn(`https://${host}:${port}/`)
  }
}
