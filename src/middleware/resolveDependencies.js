const debug = require('debug')
const Cookies = require('cookies')
const {getDependencies} = require('../helpers/getDependencies')
const {cacheDigestFilter} = require('../helpers/cacheDigestFilter')
const {checkImmutable} = require('../helpers/checkImmutable')
const {getPushPolicy} = require('../helpers/getPushPolicy')
const {getOrigin} = require('../helpers/getOrigin')
const {requestDestination} = require('../helpers/requestDestination')
const {computeDigestValue} = require('cache-digest')

const supportedPushPolicies = ['fast-load', 'default', 'head', 'none']
const HTTP2_PRIORITY_DEFAULT = 16

module.exports.resolveDependencies = () => async (request, response, next) => {
  if ((request.httpVersionMajor === 2 && !response.stream.pushAllowed) ||
    request.options.manifest.length === 0
  ) {
    return next()
  }

  const log = debug('http2server')
  const pushPolicy = getPushPolicy(request, supportedPushPolicies)
  if (pushPolicy !== undefined) {
    response.setHeader('push-policy', pushPolicy)
    if (pushPolicy === 'none') {
      return next()
    }
  }
  const method = pushPolicy === 'head' ? 'HEAD' : request.method

  const baseUrl = getOrigin(request)
  const cookies = new Cookies(request, response)
  const pushedImmutables = []
  request.pushResponses = []

  const {dependencies, priorities} = getDependencies(
    request.resolved,
    request.options.manifest,
    request.fileIndex
  )

  const cacheDigestContains = cacheDigestFilter(request, cookies, baseUrl)

  if (request.httpVersionMajor === 2 && response.stream.pushAllowed) {
    const headers = {
      ':method': method,
      ':scheme': 'https',
      ':authority': request.authority
    }
    const pushStream = (headers, weight) => new Promise((resolve, reject) => {
      const { stream } = response
      stream.pushStream(headers, (error, stream) => {
        if (error) return reject(error)
        else resolve(stream)
      })
      if (weight !== HTTP2_PRIORITY_DEFAULT) {
        stream.priority({weight, silent: true})
      }
    })
    const pushPromises = []
    for (const dependency of dependencies) {
      if (!cacheDigestContains(dependency.pathname)) continue
      headers[':path'] = encodeURI(dependency.pathname)
      log(`PUSH ${dependency.pathname}`)
      const pushPromise = pushStream(headers, priorities.get(dependency))
        .then((pushResponse) => ({ pushResponse, dependency }))
      pushPromises.push(pushPromise)
      if (checkImmutable(
        dependency.absolute,
        request.options.cacheControl.immutable
      )) {
        dependency.isImmutable = true
        if (method === 'GET') {
          pushedImmutables.push(dependency.pathname)
        }
      }
    }
    request.pushResponses = await Promise.all(pushPromises)
  } else {
    const links = []
    for (const {pathname, absolute} of dependencies) {
      if (!cacheDigestContains(pathname)) continue
      const uri = encodeURI(pathname)
      const destination = requestDestination(absolute)
      links.push(`<${uri}>; rel=preload; as=${destination}`)
    }
    response.setHeader('link', links)
  }

  if (method === 'GET') {
    if (request.headers['cache-digest']) {
      if (cookies.get('cache-digest')) {
        cookies.set('cache-digest')
      }
    }
  }

  if (pushedImmutables.length) {
    const urls = pushedImmutables.map((pathname) => [baseUrl + pathname, null])
    const digestValue = computeDigestValue(false, urls, 2 ** 7)
    const cookie = Buffer.from(digestValue).toString('base64').replace(/=+$/, '')
    cookies.set('cache-digest', cookie)
  }

  next()
}
