const {untilBefore} = require('until-before')
const {MisdirectedRequest} = require('http-errors')
const {getHost} = require('../helpers/getHost')
const {indexFiles} = require('../helpers/indexFiles')

module.exports.hostOptions = (options, files) => {
  const optionsByDomain = new Map(options.hosts.map((host) => [
    host.domain,
    host
  ]))

  const filesByDomain = new Map()
  for (const domain of Object.keys(files)) {
    const {root, index} = files[domain]
    const {trailingSlash} = optionsByDomain.get(domain).directories
    const fileIndex = indexFiles(root, index, trailingSlash)
    filesByDomain.set(domain, fileIndex)
  }

  return (request, response, next) => {
    const hostname = request.hostname = untilBefore.call(getHost(request), ':')
    if (filesByDomain.has(hostname) && optionsByDomain.has(hostname)) {
      request.fileIndex = filesByDomain.get(hostname)
      request.options = optionsByDomain.get(hostname)
      next()
    } else {
      next(new MisdirectedRequest())
    }
  }
}
