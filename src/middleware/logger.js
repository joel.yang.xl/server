const debug = require('debug')

module.exports.logger = () => {
  const log = debug('http2server')
  return (request, response, next) => {
    log(`${request.method} ${decodeURI(request.url)}`)
    next()
  }
}
