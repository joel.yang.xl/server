const {serveResponse} = require('./serveResponse')

module.exports.serveFallback = (options) => {
  const passthrough = serveResponse(options)
  return (error, request, response, next) => {
    if (request.options !== undefined) {
      const fallback = request.options.fallback[error.statusCode]
      if (fallback !== undefined) {
        const {fileIndex: {absolute}} = request
        if (absolute.has(fallback)) {
          response.statusCode = parseInt(error.statusCode)
          request.resolved = absolute.get(fallback)
          passthrough(request, response, next)
          return
        }
      }
    }
    next(error)
  }
}
