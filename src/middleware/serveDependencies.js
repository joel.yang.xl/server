const Negotiator = require('negotiator')
const {promisify} = require('util')
const {queue} = require('d3-queue')
const {getPushPolicy} = require('../helpers/getPushPolicy')
const {buildHeaders} = require('../helpers/buildHeaders')

const supportedPushPolicies = ['fast-load', 'default', 'head', 'none']

module.exports.serveDependencies = () => async (request, response, next) => {
  if (
    request.httpVersionMajor !== 2 ||
    !response.stream.pushAllowed ||
    !request.pushResponses ||
    request.pushResponses.length === 0
  ) {
    return next()
  }

  const pushQueue = queue(Math.min(
    response.stream.session.remoteSettings.maxConcurrentStreams,
    response.stream.session.localSettings.maxConcurrentStreams
  ) - 1)

  const pushPolicy = getPushPolicy(request, supportedPushPolicies)

  const immutablePatterns = request.options.cacheControl.immutable
  const supportedEncodings = new Negotiator(request).encodings()
  const fileIndex = request.fileIndex

  for (const {pushResponse, dependency} of request.pushResponses) {
    pushQueue.defer((callback) => {
      const {headers, resolved} = buildHeaders(
        dependency,
        immutablePatterns,
        supportedEncodings,
        fileIndex
      )
      headers['push-policy'] = pushPolicy
      pushResponse.respondWithFile(
        resolved.absolute,
        headers,
        {
          statCheck (stat, headers) {
            headers['content-length'] = stat.size
            const isHeadResponse = request.method === 'HEAD' ||
              headers['push-policy'] === 'head'
            if (isHeadResponse) {
              pushResponse.respond(headers, {endStream: true})
              return false
            }
          }
        }
      )
      pushResponse.on('error', () => callback())
      pushResponse.on('finish', callback)

      return {
        abort: () => {
          try {
            pushResponse.end()
          } catch (error) {
            console.error(error)
          }
        }
      }
    })
  }

  await promisify(pushQueue.awaitAll.bind(pushQueue))()
  next()
}
