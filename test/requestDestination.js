const test = require('ava')
const {Suite} = require('benchmark')
const {
  requestDestination,
  _requestDestination
} = require('../src/helpers/requestDestination')

const fixtures = [
  [['js'], 'script'],
  [['css'], 'style'],
  [['svg', 'png', 'jpg', 'jpeg', 'webp', 'ico'], 'image'],
  [['woff', 'woff2', 'ttf', 'eot', 'otf'], 'font'],
  [['mp4', 'webm'], 'video'],
  [['mp3', 'aac', 'wav', 'flac'], 'audio'],
  [['vtt'], 'track'],
  [['html', 'txt', 'json'], 'fetch'] // or should this be 'document'?
]

test('Mixed-case lookup', (t) => {
  for (const [extensions, expected] of fixtures) {
    for (const extension of extensions) {
      const cases = [
        `/foo/bar.${extension}`,
        `/foo/bar.${extension.toUpperCase()}`
      ]
      for (const given of cases) {
        const actual = requestDestination(given)
        t.is(actual, expected)
      }
    }
  }
})

test.skip('benchmark', (t) => {
  return new Promise((resolve) => {
    const benchmark = new Suite()
    .add('Pretty', () => {
      _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.js')
      _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.css')
      _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.woff')
      _requestDestination('/foo/bar/bla/bla/blaaaaaaaa.svg')
    })
    .add('Ugly', () => {
      requestDestination('/foo/bar/bla/bla/blaaaaaaaa.js')
      requestDestination('/foo/bar/bla/bla/blaaaaaaaa.css')
      requestDestination('/foo/bar/bla/bla/blaaaaaaaa.woff')
      requestDestination('/foo/bar/bla/bla/blaaaaaaaa.svg')
    })
    .on('cycle', (event) => {
      console.log(String(event.target))
    })
    .on('complete', () => {
      const fastest = benchmark.filter('fastest').map('name').shift()
      t.is(fastest, 'Ugly')
      resolve()
    })
    .run({async: true})
  })
})
