const {spawn} = require('child_process')
const {createInterface} = require('readline')
const eventToPromise = require('event-to-promise')
const {join} = require('path')
const spy = require('through2-spy')

module.exports.startServer = async ({
  command = process.execPath,
  args = [],
  options = {
    cwd: join(process.cwd(), 'test/fixtures'),
    throwOnCrash: true
  }
} = {}) => {
  const argv = ['--', '../../bin.js']
  const child = spawn(command, [...argv, ...args], options)
  const logger = spy((chunk) => {
    process.stdout.write(chunk)
  })

  function cleanup () { child.kill() }
  process.once('exit', cleanup)
  child.once('close', (code) => {
    process.removeListener('exit', cleanup)
    if (code !== 0 && code !== null && options.throwOnCrash === true) {
      throw new Error(`Server exited with error code ${code}`)
    }
  })

  const log = createInterface({input: child.stderr.pipe(logger)})
  for (
    let line = '';
    !line.includes('Server started');
    line = await eventToPromise(log, 'line')
  ) continue

  return child
}
