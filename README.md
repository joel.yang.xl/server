# @http2/server 🕸
Static HTTP/2 webserver for single page apps and progressive web apps.

- HTTP/2, HTTP/1.1, and HTTP/1.0
- Fallback for client side routing
- Auto-generate HTTPS certificate for localhost development
- Compression with Brotli, Zopfli (Gzip), and Deflate
- Server Push manifests
- Cache Digest
- CORS
- Service Workers
- HTTP to HTTPS redirect
- Immutable caching of revved files
- Scalable, multi-processor clustering

## Installation
```
npm i -g @http2/server
```

## CLI
```
http2server <command> [options]
```

#### `--version`, `-v`
Display the current version number.

#### `--help [command]`, `-h [command]`
List global or command-specific options.

### Command: `start`
Start the server

Aliases: `run`, `up`, `launch`

#### `$PORT`
The environment variable `PORT` sets the network port for incoming HTTPS connections. This overrides `https.port` in the configuration file.

The unencrypted HTTP port, used for redirects, is derived by setting the last three digits set to `080`. E.g. HTTPS `8443` -> HTTP `8080`, HTTPS `443` -> HTTP `80`.

#### `--root [directory]`
Base directory to serve static files from. Defaults to `./public`, if it exists, or `./` otherwise. This sets the `host.root` for any host that has left the option undefined the configuration file.

#### `--config [file]`
Configuration file with options. Defaults to `./http2server.config.js`, if it exists, or the default configuration otherwise. See the [Configuration](#configuration) section for details.

#### `--open`
Open browser window after starting the server. Default: `false`

#### `--loglevel [level]`
Sets the severity threshhold for log output. Choices: `silent`, `fatal`, `error`, `warn`, `info`, `debug`, or `trace`. Default: `info`

### Command: `stop`
Shuts down the server by sending it the `SIGINT` signal.

Aliases: `halt`, `exit`, `quit`, `kill`, `down`, `end`

### Command: `reload`
Gracefully restart workers with a new configuration by sending the server the `SIGHUP` signal.

Aliases: `restart`, `refresh`, `renew`, `update`, `cycle`, `load`

### Command: `test`
Loads and validates a server configuration.

Aliases: `verify`, `validate`, `check`, `configtest`, `lint`

#### `--config [file]`
Identical to the same option of the `start` command.

## Configuration

See JSON schemas and defaults in [`src/configuration`](./src/configuration) for details.

## Server Push Manifest

The manifest is a declares which files or URLs need to be pushed as dependencies for any request.

Example: Push all CSS and JS files when serving the homepage.

```js
{ glob: '**/*.html', push: ['**/*.css', '**/*.js'] }
```

The `glob` property matches the currently served file. It can be a string or an array of strings. The syntax is similar to Bash but with nice extras. See: [minimatch](https://github.com/isaacs/minimatch) and [micromatch](https://github.com/micromatch/micromatch#matching-features) for examples.

## Server Push with Cache Digests
Page load time is a largely function of latency (round trip time × delays) and aggregate volume (number × size of assets).

Latency is minimised by using HTTP/2 Server Push to deliver any necessary assets to the browser alongside the HTML. When the browser parses the HTML it does not need to make a round trip request to fetch styles, scripts, and other assets. They are already in its cache or actively being pushed by the server as quickly as network conditions allow.

Volume is reduced by using strong compression (HPACK, Brotli, etc), and by avoiding sending redundant data.

If all assets were pushed every time, a large amount of bandwidth would be wasted. HTTP/1 asset concatenation makes a tradeoff between reducing round trips (good) and re-transferring invalidated, large files (bad). For example having to re-tranfer an entire spritesheet or JavaScript bundle because of one little change.

The HTTP/1 approach was to use file signatures (Etags) and timestamps to invalidate cached responses. This requires many expensive round trips where the browser checks with the server if any files have been modified.

Cache Digests to the rescue! Using a clever technique, called Golomb-Rice Coded Bloom Filters, a compressed list of cached responses is sent by the browser to the server. Now the server can avoid pushing assets that are fresh in the browser's cache.

With Server Push and Cache Digests the best practice is to have many small files that can be cached and updated atomically, instead of large, concatenated blobs.

Browsers do not yet support cache digests natively so Service Workers and the Cache API are used to implement them. A cookie-based fallback is available for browsers that lack Service Worker support.

## HTTPS Certificate
While the HTTP/2 specification allows unencrypted connections, web browsers strictly enforce HTTPS.

If no certificate and key are provided, one pair will be auto-generated. The generated certificate is only valid for `localhost`. It is stored in `~/.http2server`. As a user convenience, the certificate is added as trusted to the operating system so browsers will accept the certificate. A password dialog may appear to confirm. This is currently only supported on macOS and Linux.

In production use [Let's Encrypt](https://letsencrypt.org) or any other trusted, signed certificate.

Intermediate certificates are stapled to the OCSP response to speed up the TLS handshake.

## Caching Policy
By default files are served with the header `cache-control: public, must-revalidate`.

File paths that match the patterns set by the `cacheControl.immutable` option are considered to never, ever change their contents. They are served with the header `cache-control: public, max-age=31536000, immutable`. This tells browsers never to revalidate these resources.

Special named immutable patterns can be used as shorthand for complex but handy patterns.
- `hex` — Matches hexadecimal hash revved files. Example: `layout-d41d8cd98f.css`
- `emoji` — Matches emoji revved files. Example: `app.⚽️.js`

## See Also
- [Unbundle](https://www.npmjs.com/package/@http2/unbundle) — Build tool for front-end JavaScript applications, designed for HTTP/2 Server Push and Cache Digests.
- [Push Demo](https://gitlab.com/sebdeckers/push-demo) — Shows how to use this tool through a practical example.

## Colophon
Made with ❤️ by Sebastiaan Deckers in 🇸🇬 Singapore.
